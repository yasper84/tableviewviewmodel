//
//  Date+Representation.swift
//  CommonExtensions
//
//  Created by Jasper Siebelink on 17/06/2018.
//  Copyright © 2018 Jasper Siebelink. All rights reserved.
//

import UIKit

public struct DateDelta {
    public let timeRepresentation: String
    public let timeValue: Int
    
    public init(timeRepresentation: String,
                timeValue: Int) {
        self.timeRepresentation = timeRepresentation
        self.timeValue = timeValue
    }
}

public extension Date {
    private struct Constants {
        static let longFormat = "d MMM yyyy - HH:mm"
        static let shortFormat = "d MMM yyyy"
    }
    
    private static let dateFormatter = DateFormatter()
    
    public var stringRepresentation: String {
        Date.dateFormatter.dateFormat = Constants.longFormat
        return Date.dateFormatter.string(from: self)
    }
    
    public var shortStringRepresentation: String {
        Date.dateFormatter.dateFormat = Constants.shortFormat
        return Date.dateFormatter.string(from: self)
    }
    
    public var timestamp64: Int64 {
        return Int64(timeIntervalSince1970)
    }
}
