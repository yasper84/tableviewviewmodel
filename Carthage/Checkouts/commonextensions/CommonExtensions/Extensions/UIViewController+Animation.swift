//
//  UIViewController+Animation.swift
//  CommonExtensions
//
//  Created by Jasper Siebelink on 02/03/2019.
//  Copyright © 2019 Jasper Siebelink. All rights reserved.
//

import UIKit

public extension UIViewController {
    public func fade(present: Bool, completion: ((Bool)-> Void)? = nil) {
        view.alpha = present ? 0 : 1
        UIView.animate(withDuration: 0.5,
                       animations: {
                            self.view.alpha = present ? 1 : 0
                       },
                       completion: completion)
    }
}
