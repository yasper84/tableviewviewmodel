//
//  Button+Action.swift
//  CommonExtensions
//
//  Created by Jasper Siebelink on 02/03/2019.
//  Copyright © 2019 Jasper Siebelink. All rights reserved.
//

import UIKit

public extension UIControl {
    public func addClosure(for controlEvents: UIControl.Event = UIControl.Event.touchUpInside,
                           closure: @escaping () -> Void) {
        let container = ClosureContainer(closure)
        addTarget(container,
                  action: #selector(ClosureContainer.invoke),
                  for: controlEvents)
        objc_setAssociatedObject(self,
                                 String(format: "%d", arc4random()),
                                 container,
                                 objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN)
    }
}

public extension UIBarButtonItem {
    public func addClosure(closure: @escaping () -> Void) {
        let container = ClosureContainer(closure)
        target = container
        action = #selector(ClosureContainer.invoke)
        objc_setAssociatedObject(self,
                                 String(format: "%d", arc4random()),
                                 container,
                                 objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN)
    }
}

private class ClosureContainer {
    let closure: () -> Void
    
    init (_ closure: @escaping () -> Void) {
        self.closure = closure
    }
    
    @objc func invoke () {
        closure()
    }
}
