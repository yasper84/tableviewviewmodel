//
//  UIView+Init.swift
//  CommonExtensions
//
//  Created by Jasper Siebelink on 02/03/2019.
//  Copyright © 2019 Jasper Siebelink. All rights reserved.
//

import UIKit


public protocol FromNib where Self: UIView {
    static func fromNib() -> Self
}

public extension FromNib where Self: UIView {
    static func fromNib() -> Self {
        let nibName = String(describing: self)
        guard let type = self as? AnyClass, let view = Bundle(for: type).loadNibNamed(nibName,
                                                                                      owner: nil,
                                                                                      options: nil)?.first as? Self else {
            fatalError("Failed to load XIB: `\(nibName)`")
        }
        return view
    }
}

extension UIView: FromNib { }
