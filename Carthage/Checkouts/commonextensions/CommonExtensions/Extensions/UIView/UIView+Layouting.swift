//
//  UIView+Layouting.swift
//  CommonExtensions
//
//  Created by Jasper Siebelink on 02/03/2019.
//  Copyright © 2019 Jasper Siebelink. All rights reserved.
//

import UIKit

public extension UIView {
    public convenience init(autoLayout: Void) {
        self.init()
        translatesAutoresizingMaskIntoConstraints = false
    }

    @discardableResult
    public func pinToSuperviewTop(constant: CGFloat? = nil,
                                  useSafeArea: Bool = false) -> NSLayoutConstraint {
        translatesAutoresizingMaskIntoConstraints = false
        guard let superview = superview else { fatalError() }

        let targetOtherConstraint: NSLayoutAnchor = { () -> NSLayoutYAxisAnchor in
            if useSafeArea, #available(iOS 11.0, *) {
                return superview.safeAreaLayoutGuide.topAnchor
            } else {
                return superview.topAnchor
            }
        }()

        let constraint = topAnchor.constraint(equalTo: targetOtherConstraint, constant: constant ?? 0)
        constraint.isActive = true
        return constraint
    }

    @discardableResult
    public func pinToSuperviewBottom(constant: CGFloat? = nil,
                                     useSafeArea: Bool = false) -> NSLayoutConstraint {
        translatesAutoresizingMaskIntoConstraints = false
        guard let superview = superview else { fatalError() }

        let targetOtherConstraint: NSLayoutAnchor = { () -> NSLayoutYAxisAnchor in
            if useSafeArea, #available(iOS 11.0, *) {
                return superview.safeAreaLayoutGuide.bottomAnchor
            } else {
                return superview.bottomAnchor
            }
        }()

        let constraint = bottomAnchor.constraint(equalTo: targetOtherConstraint, constant: constant ?? 0)
        constraint.isActive = true
        return constraint
    }

    @discardableResult
    public func pinToSuperviewLeft(constant: CGFloat? = nil,
                                   useSafeArea: Bool = false) -> NSLayoutConstraint {
        translatesAutoresizingMaskIntoConstraints = false
        guard let superview = superview else { fatalError() }

        let targetOtherConstraint: NSLayoutAnchor = { () -> NSLayoutXAxisAnchor in
            if useSafeArea, #available(iOS 11.0, *) {
                return superview.safeAreaLayoutGuide.leftAnchor
            } else {
                return superview.leftAnchor
            }
        }()

        let constraint = leftAnchor.constraint(equalTo: targetOtherConstraint, constant: constant ?? 0)
        constraint.isActive = true
        return constraint
    }

    @discardableResult
    public func pinToSuperviewRight(constant: CGFloat? = nil,
                                    useSafeArea: Bool = false) -> NSLayoutConstraint {
        translatesAutoresizingMaskIntoConstraints = false
        guard let superview = superview else { fatalError() }

        let targetOtherConstraint: NSLayoutAnchor = { () -> NSLayoutXAxisAnchor in
            if useSafeArea, #available(iOS 11.0, *) {
                return superview.safeAreaLayoutGuide.rightAnchor
            } else {
                return superview.rightAnchor
            }
        }()

        let constraint = rightAnchor.constraint(equalTo: targetOtherConstraint, constant: constant ?? 0)
        constraint.isActive = true
        return constraint
    }

    func pinTopTo(otherView: UIView) {
        translatesAutoresizingMaskIntoConstraints = false
        topAnchor.constraint(equalTo: otherView.topAnchor).isActive = true
    }

    func pinLeftTo(otherView: UIView) {
        translatesAutoresizingMaskIntoConstraints = false
        leftAnchor.constraint(equalTo: otherView.leftAnchor).isActive = true
    }

    func pinBottomTo(otherView: UIView) {
        translatesAutoresizingMaskIntoConstraints = false
        bottomAnchor.constraint(equalTo: otherView.bottomAnchor).isActive = true
    }

    func pinRightToLeftSideOf(otherView: UIView,
                              constant: CGFloat? = nil) {
        translatesAutoresizingMaskIntoConstraints = false
        rightAnchor.constraint(equalTo: otherView.leftAnchor, constant: constant ?? 0).isActive = true
    }

    func pinLeftToRightSideOf(otherView: UIView,
                              constant: CGFloat? = nil) {
        translatesAutoresizingMaskIntoConstraints = false
        leftAnchor.constraint(equalTo: otherView.rightAnchor, constant: constant ?? 0).isActive = true
    }

    func pinX(with view: UIView) {
        translatesAutoresizingMaskIntoConstraints = false
        centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
    }

    func pinY(with view: UIView) {
        translatesAutoresizingMaskIntoConstraints = false
        centerYAnchor.constraint(equalTo: view.centerYAnchor).isActive = true
    }

    func centerXInSuperview() {
        guard let superview = superview else { fatalError() }
        pinX(with: superview)
    }

    func centerYInSuperview() {
        guard let superview = superview else { fatalError() }
        pinY(with: superview)
    }

    func centerInSuperview() {
        guard let superview = superview else { fatalError() }

        pinX(with: superview)
        pinY(with: superview)
    }

    public func pinDimensions(size: CGSize) {
        pinWidth(size.width)
        pinHeight(size.height)
    }

    public func pinWidth(_ width: CGFloat) {
        translatesAutoresizingMaskIntoConstraints = false
        widthAnchor.constraint(equalToConstant: width).isActive = true
    }

    public func pinHeight(_ height: CGFloat) {
        translatesAutoresizingMaskIntoConstraints = false
        heightAnchor.constraint(equalToConstant: height).isActive = true
    }

    @discardableResult
    func pinToSuperview(offset: UIEdgeInsets? = nil) -> [NSLayoutConstraint] {
        return [
            pinToSuperviewTop(constant: offset?.top ?? 0),
            pinToSuperviewLeft(constant: offset?.left ?? 0),
            pinToSuperviewBottom(constant: offset?.bottom ?? 0),
            pinToSuperviewRight(constant: offset?.right ?? 0)
        ]
    }
    
    public func pinHeightRatio(multipier: CGFloat) {
        translatesAutoresizingMaskIntoConstraints = false
        
        addConstraint(NSLayoutConstraint(item: self,
                                         attribute: NSLayoutConstraint.Attribute.height,
                                         relatedBy: NSLayoutConstraint.Relation.equal,
                                         toItem: self,
                                         attribute: NSLayoutConstraint.Attribute.width,
                                         multiplier: multipier,
                                         constant: 0))
    }
    
    public func pinWidthRatio(multipier: CGFloat) {
        translatesAutoresizingMaskIntoConstraints = false
        
        addConstraint(NSLayoutConstraint(item: self,
                                         attribute: NSLayoutConstraint.Attribute.width,
                                         relatedBy: NSLayoutConstraint.Relation.equal,
                                         toItem: self,
                                         attribute: NSLayoutConstraint.Attribute.height,
                                         multiplier: multipier,
                                         constant: 0))
    }
}
