//
//  UINavigationController+Extensions.swift
//  CommonExtensions
//
//  Created by Jasper Siebelink on 02/03/2019.
//  Copyright © 2019 Jasper Siebelink. All rights reserved.
//

import UIKit

// MARK: Extensions
public extension UINavigationController {
    public func fadePush(viewController: UIViewController) {
        addTransition()
        pushViewController(viewController,
                           animated: false)
    }

    public func fadePop() {
        addTransition()
        popViewController(animated: false)
    }

    private func addTransition() {
        let transition = CATransition()
        transition.duration = 0.5
        transition.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.linear)
        transition.type = CATransitionType.fade
        view.layer.add(transition, forKey: nil)
    }
}
