//
//  AnimationAnimator.swift
//  CommonExtensions
//
//  Created by Jasper Siebelink on 25/11/2018.
//  Copyright © 2018 Jasper Siebelink. All rights reserved.
//

import UIKit

public class AnimationAnimator: NSObject, UIViewControllerAnimatedTransitioning {
    public func transitionDuration(using transitionContext: UIViewControllerContextTransitioning?) -> TimeInterval {
        fatalError("Implement \(#function) by subclass")
    }
    
    public func animateTransition(using transitionContext: UIViewControllerContextTransitioning) {
        fatalError("Implement \(#function) by subclass")
    }
    
    public var presenting: Bool = true
}
