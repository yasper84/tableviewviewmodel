//
//  AnimationNavigationController.swift
//  CommonExtensions
//
//  Created by Jasper Siebelink on 26/11/2018.
//  Copyright © 2018 Jasper Siebelink. All rights reserved.
//

import UIKit

public final class AnimationNavigationController: UINavigationController, AnimationDestinationViewControllerProtocol {
    public var animationAnimator: AnimationAnimator?
    
    public override init(rootViewController: UIViewController) {
        super.init(rootViewController: rootViewController)
        commonInit()
    }
    
    public required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    public override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
    }
    
    private func commonInit() {
        setNavigationBarHidden(true,
                               animated: false)
    }
}
