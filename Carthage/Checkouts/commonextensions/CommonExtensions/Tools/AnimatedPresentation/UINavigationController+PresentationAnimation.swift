//
//  UINavigationController+PresentationAnimation.swift
//  CommonExtensions
//
//  Created by Jasper Siebelink on 25/11/2018.
//  Copyright © 2018 Jasper Siebelink. All rights reserved.
//

import UIKit

public extension UINavigationController {
    enum NavigationAnimation: Int {
        case fadePresent        = 1
        case slowFadePresent    = 2
        case zoomPresent        = 3
    }
    
    struct Constants {
        static let customAnimationDuration: TimeInterval = 0.2
        static let slowAnimationDuration: TimeInterval = 0.5
    }
    
    public func presentAnimated(viewController: UIViewController,
                                animationType: NavigationAnimation,
                                completion: (() -> Void)? = nil) {
        view.tag = animationType.rawValue
        
        let viewControllerToPresent = (viewController is UINavigationController) ?
            viewController :
            AnimationNavigationController(rootViewController: viewController)
        
        viewControllerToPresent.modalPresentationStyle = .overCurrentContext
        viewControllerToPresent.transitioningDelegate = self
        
        present(viewControllerToPresent,
                animated: true,
                completion: completion)
    }
}


// MARK: Fade present
public extension UINavigationController {
    public func fadePresent(_ viewController: UIViewController) {
        presentAnimated(viewController: viewController,
                        animationType: NavigationAnimation.fadePresent,
                        completion: nil)
    }
    
    public func slowFadePresent(_ viewController: UIViewController) {
        presentAnimated(viewController: viewController,
                        animationType: NavigationAnimation.slowFadePresent,
                        completion: nil)
    }
    
    private func fadePresent(presentedViewController: UIViewController,
                             presentingViewController: UIViewController,
                             duration: TimeInterval = UINavigationController.Constants.customAnimationDuration) -> UIViewControllerAnimatedTransitioning? {
        return FadePresentAnimator(animationSpeed: duration)
    }
}

// MARK: Zoom present
public extension UINavigationController {
    public func zoomViewControllerIn(_ viewController: UIViewController,
                                     completion: (() -> Void)? = nil) {
        presentAnimated(viewController: viewController,
                        animationType: NavigationAnimation.zoomPresent,
                        completion: completion)
    }
    
    private func zoomPresent(presentedViewController: UIViewController,
                             presentingViewController: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        let zoomOriginViewController: ZoomOriginViewControllerProtocol
        if let navVC = presentingViewController as? UINavigationController,
            let zoomOriginVC = navVC.viewControllers.last as? ZoomOriginViewControllerProtocol {
            zoomOriginViewController = zoomOriginVC
        } else if let zoomOriginVC = presentingViewController as? ZoomOriginViewControllerProtocol {
            zoomOriginViewController = zoomOriginVC
        } else {
            logError("Failed to identify ZoomOriginViewControllerProtocol")
            return nil
        }
        return ZoomInAnimator(zoomOriginViewController: zoomOriginViewController)
    }
}

// MARK: UIViewControllerTransitioningDelegate
extension UINavigationController: UIViewControllerTransitioningDelegate {
    public func animationController(forPresented presented: UIViewController,
                                    presenting: UIViewController,
                                    source: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        guard let navigationAnimationType = NavigationAnimation(rawValue: view.tag),
            var animationDestinationViewController = presented as? AnimationDestinationViewControllerProtocol else { return nil }
        
        var transitioningAnimator: UIViewControllerAnimatedTransitioning?
        switch navigationAnimationType {
        case .zoomPresent:
            transitioningAnimator = zoomPresent(presentedViewController: presented,
                                                presentingViewController: presenting)
        case .fadePresent:
            transitioningAnimator = fadePresent(presentedViewController: presented,
                                                presentingViewController: presenting)
        case .slowFadePresent:
            transitioningAnimator = fadePresent(presentedViewController: presented,
                                                presentingViewController: presenting,
                                                duration: UINavigationController.Constants.slowAnimationDuration)
        }
        
        animationDestinationViewController.animationAnimator = (transitioningAnimator as? AnimationAnimator)
        return transitioningAnimator
        
    }
    
    public func animationController(forDismissed dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        guard let animationAnimator = (dismissed as? AnimationDestinationViewControllerProtocol)?.animationAnimator else { return nil }
        animationAnimator.presenting = false
        return animationAnimator
    }
}
