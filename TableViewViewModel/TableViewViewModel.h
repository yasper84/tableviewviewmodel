//
//  TableViewViewModel.h
//  TableViewViewModel
//
//  Created by Jasper Siebelink on 02/03/2019.
//  Copyright © 2019 Jasper Siebelink. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for TableViewViewModel.
FOUNDATION_EXPORT double TableViewViewModelVersionNumber;

//! Project version string for TableViewViewModel.
FOUNDATION_EXPORT const unsigned char TableViewViewModelVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <TableViewViewModel/PublicHeader.h>


