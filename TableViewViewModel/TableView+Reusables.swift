//
//  TableView+Registration.swift
//  TableViewViewModel
//
//  Created by Jasper Siebelink on 06/07/2019.
//  Copyright © 2019 Jasper Siebelink. All rights reserved.
//

import UIKit

extension UITableView {
    public func reusableCell<T: UITableViewCell>(type: T.Type, for indexPath: IndexPath? = nil) -> T {
        let cell: T?
        let className = String(describing: type)
        if let indexPath = indexPath {
            cell = self.dequeueReusableCell(withIdentifier: className, for: indexPath) as? T
        } else {
            cell = self.dequeueReusableCell(withIdentifier: className) as? T
        }
        guard let dequeuedCell = cell else {
            fatalError("Unable to dequeue cell: \(className)")
        }
        return dequeuedCell
    }
    
    public func reusableHeaderFooter<T: UITableViewHeaderFooterView>(type: T.Type) -> T {
        let className = String(describing: type)
        guard let dequeuedView = dequeueReusableHeaderFooterView(withIdentifier: className) as? T else {
            fatalError("Unable to dequeue view: \(className)")
        }
        return dequeuedView
    }
    
    public func registerCells<T: UIView>(types: [T.Type]) {
        types.forEach { classType in
            if let classType = classType as? UITableViewCell.Type {
                registerCell(type: classType)
                return
            }
            if let classType = classType as? UITableViewHeaderFooterView.Type {
                registerCell(type: classType)
                return
            }
        }
    }
    
    public func registerCell<T: UITableViewCell>(type: T.Type) {
        let identifier = String(describing: type)
        let bundle = Bundle(for: type)
        if bundle.path(forResource: identifier, ofType: "nib") != nil {
            let nib = UINib(nibName: identifier, bundle: bundle)
            self.register(nib, forCellReuseIdentifier: identifier)
            return
        }
        self.register(type, forCellReuseIdentifier: identifier)
    }
    
    public func registerCell<T: UITableViewHeaderFooterView>(type: T.Type) {
        let identifier = String(describing: type)
        let bundle = Bundle(for: type)
        if bundle.path(forResource: identifier, ofType: "nib") != nil {
            let nib = UINib(nibName: identifier, bundle: bundle)
            self.register(nib, forHeaderFooterViewReuseIdentifier: identifier)
            return
        }
        self.register(type, forHeaderFooterViewReuseIdentifier: identifier)
    }
}
