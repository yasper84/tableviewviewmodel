//
//  GenericList
//
//  Created by Jasper Siebelink on 22/01/2019.
//  Copyright © 2019 Jasper Siebelink. All rights reserved.
//

import UIKit

// MARK: UITableViewCell configuration
// To be implemented by cells used in the configuration
public protocol ConfigurableTableViewCell where Self: UITableViewCell {
    associatedtype ViewModel: Equatable
    func configure(with model: ViewModel)
}

// Type Erasure
public protocol TableViewCellConfigurationProtocol {
    var cellType: UITableViewCell.Type { get }
    var height: CGFloat? { get }
    
    func configure(_ cell: UITableViewCell)
    func cellClicked(indexPath: IndexPath)
    func equals(other: TableViewCellConfigurationProtocol) -> Bool
}

// Responsible for linking cell with cellviewmodel
public struct TableViewCellConfiguration<T: ConfigurableTableViewCell>: TableViewCellConfigurationProtocol {
    public typealias OnCellClicked = ((T.ViewModel, IndexPath) -> Void)
    
    public let cellViewModel: T.ViewModel?
    public let type: T.Type
    public let onCellClicked: OnCellClicked?
    public let height: CGFloat?
    
    public init(cellViewModel: T.ViewModel?,
                type: T.Type,
                onCellClicked: OnCellClicked? = nil,
                height: CGFloat? = nil) {
        self.cellViewModel = cellViewModel
        self.type = type
        self.onCellClicked = onCellClicked
        self.height = height
    }
    
    public var cellType: UITableViewCell.Type {
        return type
    }
    
    public func configure(_ cell: UITableViewCell) {
        guard let cellViewModel = cellViewModel else { return }
        (cell as? T)?.configure(with: cellViewModel)
    }
    
    public func cellClicked(indexPath: IndexPath) {
        guard let cellViewModel = cellViewModel else { return }
        onCellClicked?(cellViewModel, indexPath)
    }
    
    public func equals(other: TableViewCellConfigurationProtocol) -> Bool {
        guard let otherTyped = other as? TableViewCellConfiguration<T> else { return false}
        return self == otherTyped
    }
    
    public static func == (lhs: TableViewCellConfiguration<T>, rhs: TableViewCellConfiguration<T>) -> Bool {
        return lhs.cellViewModel == rhs.cellViewModel
    }
}

// MARK: HeaderFooter configuration
// To be implemented by headers/footers used in the configuration
public protocol ConfigurableTableViewHeaderFooter where Self: UITableViewHeaderFooterView {
    associatedtype ViewModel: Equatable
    func configure(with model: ViewModel)
}

// Type Erasure
public protocol TableViewHeaderFooterConfigurationProtocol {
    var headerFooterType: UITableViewHeaderFooterView.Type { get }
    func configure(_ headerFooter: UITableViewHeaderFooterView)
    func equals(other: TableViewHeaderFooterConfigurationProtocol) -> Bool
}

// Responsible for linking header/footer with header/footer viewmodel
public struct TableViewHeaderFooterConfiguration<T: ConfigurableTableViewHeaderFooter>: TableViewHeaderFooterConfigurationProtocol {
    let headerFooterViewModel: T.ViewModel?
    let type: T.Type
    
    public init(viewModel: T.ViewModel?,
                type: T.Type) {
        self.headerFooterViewModel = viewModel
        self.type = type
    }
    
    public var headerFooterType: UITableViewHeaderFooterView.Type {
        return type
    }
    
    public func configure(_ headerFooter: UITableViewHeaderFooterView) {
        guard let viewModel = headerFooterViewModel else { return }
        (headerFooter as? T)?.configure(with: viewModel)
    }
    
    public func equals(other: TableViewHeaderFooterConfigurationProtocol) -> Bool {
        guard let otherTyped = other as? TableViewHeaderFooterConfiguration<T> else { return false}
        return self == otherTyped
    }
    
    public static func == (lhs: TableViewHeaderFooterConfiguration<T>, rhs: TableViewHeaderFooterConfiguration<T>) -> Bool {
        return lhs.headerFooterViewModel == rhs.headerFooterViewModel
    }
}
