//
//  TableViewSectionViewModel
//  GenericList
//
//  Created by Jasper Siebelink on 22/01/2019.
//  Copyright © 2019 Jasper Siebelink. All rights reserved.
//

import UIKit

public struct TableViewSectionViewModel: Equatable {
    public var cellConfigurations: [TableViewCellConfigurationProtocol]
    public var headerConfiguration: TableViewHeaderFooterConfigurationProtocol?
    public var footerConfiguration: TableViewHeaderFooterConfigurationProtocol?
    public let sectionKey: String? // show section indexes on the right of the tableview
    public var comparisonKey: String? // Optional ID used to compare sections when updating - not for UI purposes.
    
    // MARK: Initialization
    public init() {
        cellConfigurations = []
        sectionKey = nil
        
    }
    
    // Init with prepared cell configurations
    public init<T: ConfigurableTableViewCell>(cellConfigurations: [TableViewCellConfiguration<T>],
                                              cellHeight: CGFloat? = nil,
                                              footer: UIView? = nil,
                                              sectionKey: String? = nil,
                                              comparisonKey: String? = nil) {
        self.cellConfigurations = cellConfigurations
        self.sectionKey = sectionKey
        self.comparisonKey = comparisonKey
    }
    
    // Initializer with header
    public init<
        T: ConfigurableTableViewCell,
        H: ConfigurableTableViewHeaderFooter>(headerViewModel: H.ViewModel,
                                              headerType: H.Type,
                                              viewModels: [T.ViewModel],
                                              cellType: T.Type,
                                              cellHeight: CGFloat? = nil,
                                              onCellClicked: ((T.ViewModel, IndexPath) -> Void)? = nil,
                                              sectionKey: String? = nil,
                                              comparisonKey: String? = nil) {
        self.init(viewModels: viewModels,
                  cellType: cellType,
                  cellHeight: cellHeight,
                  onCellClicked: onCellClicked,
                  sectionKey: sectionKey,
                  comparisonKey: comparisonKey)
        
        setHeader(headerViewModel: headerViewModel,
                  headerType: headerType)
    }
    
    // Initializer with footer
    public init<
        T: ConfigurableTableViewCell,
        F: ConfigurableTableViewHeaderFooter>(viewModels: [T.ViewModel],
                                              cellType: T.Type,
                                              cellHeight: CGFloat? = nil,
                                              onCellClicked: ((T.ViewModel, IndexPath) -> Void)? = nil,
                                              sectionKey: String? = nil,
                                              footerViewModel: F.ViewModel,
                                              footerType: F.Type,
                                              comparisonKey: String? = nil) {
        self.init(viewModels: viewModels,
                  cellType: cellType,
                  cellHeight: cellHeight,
                  onCellClicked: onCellClicked,
                  sectionKey: sectionKey,
                  comparisonKey: comparisonKey)
        
        setFooter(footerViewModel: footerViewModel,
                  footerType: footerType)
    }
    
    // Initializer with header&footer
    public init<
        T: ConfigurableTableViewCell,
        H: ConfigurableTableViewHeaderFooter,
        F: ConfigurableTableViewHeaderFooter>(headerViewModel: H.ViewModel,
                                              headerType: H.Type,
                                              viewModels: [T.ViewModel],
                                              cellType: T.Type,
                                              cellHeight: CGFloat? = nil,
                                              onCellClicked: ((T.ViewModel, IndexPath) -> Void)? = nil,
                                              sectionKey: String? = nil,
                                              footerViewModel: F.ViewModel,
                                              footerType: F.Type,
                                              comparisonKey: String? = nil) {
        self.init(viewModels: viewModels,
                  cellType: cellType,
                  cellHeight: cellHeight,
                  onCellClicked: onCellClicked,
                  sectionKey: sectionKey,
                  comparisonKey: comparisonKey)
        
        setHeader(headerViewModel: headerViewModel,
                  headerType: headerType)
        setFooter(footerViewModel: footerViewModel,
                  footerType: footerType)
    }
    
    // Init without header or footer
    public init<T: ConfigurableTableViewCell>(viewModels: [T.ViewModel],
                                              cellType: T.Type,
                                              cellHeight: CGFloat? = nil,
                                              onCellClicked: ((T.ViewModel, IndexPath) -> Void)? = nil,
                                              sectionKey: String? = nil,
                                              comparisonKey: String? = nil) {
        self.sectionKey = sectionKey
        self.comparisonKey = comparisonKey
        
        cellConfigurations = viewModels.map({ (viewModel) -> TableViewCellConfigurationProtocol in
            return TableViewCellConfiguration<T>(cellViewModel: viewModel,
                                                 type: cellType,
                                                 onCellClicked: onCellClicked,
                                                 height: cellHeight)
        })
    }
    
    // MARK: Mutation
    public mutating func setHeader<H: ConfigurableTableViewHeaderFooter>(headerViewModel: H.ViewModel,
                                                                         headerType: H.Type) {
        headerConfiguration = TableViewHeaderFooterConfiguration(viewModel: headerViewModel,
                                                                 type: headerType)
    }
    
    public mutating func setFooter<F: ConfigurableTableViewHeaderFooter>(footerViewModel: F.ViewModel,
                                                                         footerType: F.Type) {
        footerConfiguration = TableViewHeaderFooterConfiguration(viewModel: footerViewModel,
                                                                 type: footerType)
    }
    
    public mutating func append<T: ConfigurableTableViewCell>(viewModel: T.ViewModel,
                                                              cellType: T.Type,
                                                              cellHeight: CGFloat? = nil,
                                                              onCellClicked: ((T.ViewModel, IndexPath) -> Void)? = nil) {
        let newConfiguration = TableViewCellConfiguration<T>(cellViewModel: viewModel,
                                                             type: cellType,
                                                             onCellClicked: onCellClicked,
                                                             height: cellHeight)
        cellConfigurations.append(newConfiguration)
    }
    
    public mutating func append<T: ConfigurableTableViewCell>(viewModels: [T.ViewModel],
                                                              cellType: T.Type,
                                                              cellHeight: CGFloat? = nil,
                                                              onCellClicked: ((T.ViewModel, IndexPath) -> Void)? = nil) {
        viewModels.forEach { (viewModel) in
            append(viewModel: viewModel,
                   cellType: cellType,
                   cellHeight: cellHeight,
                   onCellClicked: onCellClicked)
        }
    }
    
    public static func == (lhs: TableViewSectionViewModel, rhs: TableViewSectionViewModel) -> Bool {
        // Attempt to determine entries are equal based on optional sectionkey
        if let lhsSectionKey = lhs.sectionKey, let rhsSectionKey = rhs.sectionKey {
            return lhsSectionKey == rhsSectionKey
        }
        
        // Attempt to determine entries are equal based on optional headerComparisonKey
        if let lhsSectionKey = lhs.comparisonKey, let rhsSectionKey = rhs.comparisonKey {
            return lhsSectionKey == rhsSectionKey
        }
        
        // Try to do it based on optional headerviewmodel
        if let lhsHeaderConfig = lhs.headerConfiguration, let rhsHeaderConfig = rhs.headerConfiguration {
            return lhsHeaderConfig.equals(other: rhsHeaderConfig)
        }
        
        // As a final attempt, check to see if the equatable cellviewmodels match exactly
        let lhsConfigurations: [TableViewCellConfigurationProtocol] = lhs.cellConfigurations
        let rhsConfigurations: [TableViewCellConfigurationProtocol] = rhs.cellConfigurations
        guard lhsConfigurations.count == rhsConfigurations.count else { return false }
        let indices: [Int] =  Array(0..<lhs.cellConfigurations.count)
        let collectionsMatch: Bool =
            indices.reduce(true, { (previousResult, position) -> Bool in
                return previousResult && lhsConfigurations[position].equals(other: rhsConfigurations[position])
            })
        
        return collectionsMatch
    }
}
