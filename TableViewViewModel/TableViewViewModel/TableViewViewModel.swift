//
//  TableViewViewModel.swift
//  GenericList
//
//  Created by Jasper Siebelink on 22/01/2019.
//  Copyright © 2019 Jasper Siebelink. All rights reserved.
//

import UIKit
import CommonExtensions

open class TableViewViewModel: NSObject {
    public var sectionsViewModels: [TableViewSectionViewModel] = []
    
    public weak var tableView: UITableView?
    
    public init(sections: [TableViewSectionViewModel]? = nil) {
        super.init()
        
        if let sections = sections {
            self.sectionsViewModels = sections
        }
    }
    
    open func configure(tableView: UITableView) {
        self.tableView = tableView
        
        // If this instance is set to the TableView after the SectionViewModels were added,
        // the cells won't be registered yet
        sectionsViewModels.forEach { (sectionViewModel) in
            sectionViewModel.cellConfigurations.forEach({ (cellConfiguration) in
                tableView.registerCell(type: cellConfiguration.cellType)
            })
            
            if let headerConfiguration = sectionViewModel.headerConfiguration {
                tableView.registerCell(type: headerConfiguration.headerFooterType)
            }
            
            if let footerConfiguration = sectionViewModel.footerConfiguration {
                tableView.registerCell(type: footerConfiguration.headerFooterType)
            }
        }
        
        tableView.delegate = self
        tableView.dataSource = self
        tableView.estimatedRowHeight = 44   // Required for iOS 10
        tableView.estimatedSectionHeaderHeight = 44   // Required for iOS 10
        tableView.estimatedSectionFooterHeight = UITableView.automaticDimension
        tableView.sectionHeaderHeight = UITableView.automaticDimension
        
        tableView.reloadData()
    }
    
    open func reset() {
        sectionsViewModels.removeAll()
    }
    
    open func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        tableView?.endEditing(true)
    }
}

extension TableViewViewModel: UITableViewDataSource {
    open func numberOfSections(in tableView: UITableView) -> Int {
        return sectionsViewModels.count
    }
    
    open func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return sectionsViewModels[section].cellConfigurations.count
    }
    
    open func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellConfiguration: TableViewCellConfigurationProtocol = sectionsViewModels[indexPath.section].cellConfigurations[indexPath.row]
        let className = cellConfiguration.cellType.className
        let cell = tableView.dequeueReusableCell(withIdentifier: className,
                                                 for: indexPath)
        cell.selectionStyle = UITableViewCell.SelectionStyle.none
        cellConfiguration.configure(cell)
        
        return cell
    }
    
    open func sectionIndexTitles(for tableView: UITableView) -> [String]? {
        guard !sectionsViewModels.isEmpty else { return nil }
        let sectionIndices: [String] = sectionsViewModels.compactMap { (sectionViewModel) -> String? in
            return sectionViewModel.sectionKey
        }
        
        guard sectionIndices.count == sectionsViewModels.count else { return nil }
        return sectionIndices
    }
}

extension TableViewViewModel: UITableViewDelegate {
    open func tableView(_ tableView: UITableView,
                        heightForRowAt indexPath: IndexPath) -> CGFloat {
        return sectionsViewModels[indexPath.section]
            .cellConfigurations[indexPath.row].height ?? UITableView.automaticDimension
    }
    
    open func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        guard let headerConfiguration: TableViewHeaderFooterConfigurationProtocol =
            sectionsViewModels[section].headerConfiguration else { return nil }
        let headerView = tableView.reusableHeaderFooter(type: headerConfiguration.headerFooterType)
        headerConfiguration.configure(headerView)
        return headerView
    }
    
    open func tableView(_ tableView: UITableView,
                        heightForHeaderInSection section: Int) -> CGFloat {
        return sectionsViewModels[section].headerConfiguration != nil ?
            UITableView.automaticDimension :
            CGFloat.leastNonzeroMagnitude
    }
    
    open func tableView(_ tableView: UITableView,
                        viewForFooterInSection section: Int) -> UIView? {
        guard let footerConfiguration: TableViewHeaderFooterConfigurationProtocol =
            sectionsViewModels[section].footerConfiguration else { return nil }
        let footerView = tableView.reusableHeaderFooter(type: footerConfiguration.headerFooterType)
        footerConfiguration.configure(footerView)
        return footerView
    }
    
    open func tableView(_ tableView: UITableView,
                        heightForFooterInSection section: Int) -> CGFloat {
        return sectionsViewModels[section].footerConfiguration != nil ?
            UITableView.automaticDimension :
            CGFloat.leastNonzeroMagnitude
    }
    
    open func tableView(_ tableView: UITableView,
                        didSelectRowAt indexPath: IndexPath) {
        sectionsViewModels[indexPath.section].cellConfigurations[indexPath.row].cellClicked(indexPath: indexPath)
    }
}
