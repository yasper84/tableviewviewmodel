//
//  TableView+ConfiguratorManipulation.swift
//  GenericList
//
//  Created by Jasper Siebelink on 21/01/2019.
//  Copyright © 2019 Jasper Siebelink. All rights reserved.
//

import UIKit

// MARK: Section addition
extension TableViewViewModel {
    public func append(section: TableViewSectionViewModel,
                       reloadAnimation: UITableView.RowAnimation? = UITableView.RowAnimation.none) {
        registerCellsInSection(section)
        
        sectionsViewModels.append(section)
        
        guard let reloadAnimation = reloadAnimation else { return }
        performTableMutations(mutations: {
            self.tableView?.insertSections(IndexSet([self.sectionsViewModels.count-1]),
                                           with: reloadAnimation)
        }, reloadAnimation: reloadAnimation)
    }
    
    public func append(sections: [TableViewSectionViewModel],
                       reloadAnimation: UITableView.RowAnimation? = UITableView.RowAnimation.none) {
        let nextSectionPosition: Int = sectionsViewModels.count
        sections.forEach({
            append(section: $0,
                   reloadAnimation: nil)
        })
        
        let targetEndIndex: Int = sectionsViewModels.count-1
        guard let reloadAnimation = reloadAnimation, targetEndIndex >= nextSectionPosition else { return }
        let arrayToUpdate: [Int] = Array(nextSectionPosition...targetEndIndex)
        performTableMutations(mutations: {
            self.tableView?.insertSections(IndexSet(arrayToUpdate),
                                           with: reloadAnimation)
        }, reloadAnimation: reloadAnimation)
    }
}

// MARK: Section removal
extension TableViewViewModel {
    public func remove(sectionIndex: Int,
                       reloadAnimation: UITableView.RowAnimation? = UITableView.RowAnimation.none) {
        sectionsViewModels.remove(at: sectionIndex)
        
        guard let reloadAnimation = reloadAnimation else { return }
        performTableMutations(mutations: {
            self.tableView?.deleteSections(IndexSet([sectionIndex]),
                                           with: reloadAnimation)
        }, reloadAnimation: reloadAnimation)
    }
    
    public func remove(sectionIndices: [Int],
                       reloadAnimation: UITableView.RowAnimation? = UITableView.RowAnimation.none) {
        sectionIndices.reversed().forEach({ remove(sectionIndex: $0,
                                                   reloadAnimation: nil) })
        
        guard let reloadAnimation = reloadAnimation else { return }
        performTableMutations(mutations: {
            self.tableView?.deleteSections(IndexSet(sectionIndices),
                                           with: reloadAnimation)
        }, reloadAnimation: reloadAnimation)
    }
    
    public func removeAllSections() {
        let sectionIndices: [Int] = Array(0..<sectionsViewModels.count)
        remove(sectionIndices: sectionIndices)
    }
}

// MARK: Section update
extension TableViewViewModel {
    // Replace an entire section, optionally animated
    public func updateSection(newSectionViewModel: TableViewSectionViewModel,
                              sectionIndex: Int = 0,
                              reloadAnimation: UITableView.RowAnimation? = UITableView.RowAnimation.none) {
        // If there is no section yet, just append it
        guard sectionsViewModels.count > sectionIndex else {
            append(section: newSectionViewModel,
                   reloadAnimation: reloadAnimation)
            return
        }
        
        // Determine what to insert and delete based on Equatable cellViewModels
        let existingItems: [TableViewCellConfigurationProtocol] = sectionsViewModels[sectionIndex].cellConfigurations
        let newItems: [TableViewCellConfigurationProtocol] = newSectionViewModel.cellConfigurations
        sectionsViewModels[sectionIndex] = newSectionViewModel
        
        // Register the cells
        registerCellsInSection(newSectionViewModel)
        
        // If we don't animate
        guard let reloadAnimation = reloadAnimation else { return }
        
        var rowsToInsert: [IndexPath] = []
        var rowsToDelete: [IndexPath] = []
        
        for (index, existingCellConfiguration) in existingItems.enumerated() {
            guard !newItems.contains(where: { $0.equals(other: existingCellConfiguration) }) else { continue }
            rowsToDelete.append(IndexPath(row: index,
                                          section: sectionIndex))
        }
        
        for (index, newCellConfiguration) in newItems.enumerated() {
            guard !existingItems.contains(where: { $0.equals(other: newCellConfiguration) }) else { continue }
            rowsToInsert.append(IndexPath(row: index,
                                          section: sectionIndex))
        }
        
        // Animate the new configuration
        guard !(rowsToInsert.isEmpty && rowsToDelete.isEmpty) else { return }
        performTableMutations(mutations: {
            self.tableView?.deleteRows(at: rowsToDelete,
                                       with: reloadAnimation)
            self.tableView?.insertRows(at: rowsToInsert,
                                       with: reloadAnimation)
        }, reloadAnimation: reloadAnimation)
    }
    
    public func updateSections(_ newSections: [TableViewSectionViewModel],
                               reloadAnimation: UITableView.RowAnimation? = UITableView.RowAnimation.fade) {
        if sectionsViewModels.isEmpty {
            append(sections: newSections,
                   reloadAnimation: reloadAnimation)
        } else {
            let existingSections: [TableViewSectionViewModel] = sectionsViewModels
            
            // First check if we can update any existing sections
            for (sectionIndex, existingSectionConfiguration) in existingSections.enumerated() {
                if let newMatchingSection = newSections.first(where: {
                    $0 == existingSectionConfiguration
                }) {
                    // If it already existed and still exists, see if any of the rows changed
                    updateSection(newSectionViewModel: newMatchingSection,
                                  sectionIndex: sectionIndex,
                                  reloadAnimation: reloadAnimation)
                }
            }
            
            // Next check to bulk-insert and delete
            var sectionsToInsert: [Int] = []
            var sectionsToDelete: [Int] = []
            
            // Check the existing sections and see if they are present in the new configuration
            for (sectionIndex, existingSectionConfiguration) in existingSections.enumerated() {
                if !newSections.contains(existingSectionConfiguration) {
                    // If it doesn't exist any more, delete it
                    sectionsToDelete.append(sectionIndex)
                }
            }
            
            // We can only replace this now, as `replaceSection` needed the original configuration
            sectionsViewModels = newSections
            
            // Check the new sections and see if they are present in the old configuration
            // If not, insert it
            for (sectionIndex, newSectionConfiguration) in newSections.enumerated() {
                if !existingSections.contains(where: { $0 == newSectionConfiguration }) {
                    registerCellsInSection(newSectionConfiguration)
                    sectionsToInsert.append(sectionIndex)
                }
            }
            
            // Animate the new configuration
            guard
                let reloadAnimation = reloadAnimation,
                !(sectionsToDelete.isEmpty && sectionsToInsert.isEmpty) else { return }
            performTableMutations(mutations: {
                self.tableView?.deleteSections(IndexSet(sectionsToDelete),
                                               with: reloadAnimation)
                self.tableView?.insertSections(IndexSet(sectionsToInsert),
                                               with: reloadAnimation)
            }, reloadAnimation: reloadAnimation)
        }
    }
}

// MARK: Row addition, removal
extension TableViewViewModel {
    public func append<T: ConfigurableTableViewCell>(cellViewModel: T.ViewModel,
                                                     cellType: T.Type,
                                                     onCellClicked: TableViewCellConfiguration<T>.OnCellClicked? = nil,
                                                     cellHeight: CGFloat? = nil,
                                                     indexPath: IndexPath? = nil,
                                                     reloadAnimation: UITableView.RowAnimation? = UITableView.RowAnimation.none) {
        let cellConfiguation = TableViewCellConfiguration(cellViewModel: cellViewModel,
                                                          type: cellType,
                                                          onCellClicked: onCellClicked,
                                                          height: cellHeight)
        append(cellConfigurator: cellConfiguation,
               indexPath: indexPath,
               reloadAnimation: reloadAnimation)
    }
    
    public func append<T: ConfigurableTableViewCell>(cellConfigurator: TableViewCellConfiguration<T>,
                                                     indexPath: IndexPath? = nil,
                                                     reloadAnimation: UITableView.RowAnimation? = UITableView.RowAnimation.none) {
        tableView?.registerCell(type: cellConfigurator.cellType)
        
        let rowIndexPath: IndexPath
        if let indexPath = indexPath {
            rowIndexPath = indexPath
        } else {
            guard !sectionsViewModels.isEmpty else {
                append(section: TableViewSectionViewModel(cellConfigurations: [cellConfigurator]),
                       reloadAnimation: reloadAnimation)
                return
            }
            
            let sectionPosition: Int = sectionsViewModels.isEmpty ? 0 : sectionsViewModels.count - 1
            let newRowPosition: Int = sectionsViewModels.isEmpty ? 0 : sectionsViewModels[sectionPosition].cellConfigurations.count
            rowIndexPath = IndexPath(row: newRowPosition,
                                     section: sectionPosition)
        }
        
        var sectionViewModel = sectionsViewModels[rowIndexPath.section]
        sectionViewModel.cellConfigurations.insert(cellConfigurator,
                                                   at: rowIndexPath.row)
        sectionsViewModels[rowIndexPath.section] = sectionViewModel
        
        guard let reloadAnimation = reloadAnimation else { return }
        performTableMutations(mutations: {
            self.tableView?.insertRows(at: [rowIndexPath],
                                       with: reloadAnimation)
        }, reloadAnimation: reloadAnimation)
    }
    
    public func remove(indexPath: IndexPath,
                       reloadAnimation: UITableView.RowAnimation? = UITableView.RowAnimation.none) {
        var sectionViewModel: TableViewSectionViewModel = sectionsViewModels[indexPath.section]
        sectionViewModel.cellConfigurations.remove(at: indexPath.row)
        sectionsViewModels[indexPath.section] = sectionViewModel
        
        guard let reloadAnimation = reloadAnimation else { return }
        performTableMutations(mutations: {
            self.tableView?.deleteRows(at: [indexPath],
                                       with: reloadAnimation)
        }, reloadAnimation: reloadAnimation)
    }
    
    public func remove(indices: [IndexPath],
                       reloadAnimation: UITableView.RowAnimation? = UITableView.RowAnimation.none) {
        indices.forEach({ remove(indexPath: $0,
                                 reloadAnimation: nil) })
        
        guard let reloadAnimation = reloadAnimation else { return }
        performTableMutations(mutations: {
            self.tableView?.deleteRows(at: indices,
                                       with: reloadAnimation)
        }, reloadAnimation: reloadAnimation)
    }
}

// MARK: Support
private extension TableViewViewModel {
    
    func registerCellsInSection(_ section: TableViewSectionViewModel) {
        section.cellConfigurations.forEach({ registerCellType($0.cellType) })
        
        if let headerConfiguration = section.headerConfiguration {
            tableView?.registerCell(type: headerConfiguration.headerFooterType)
        }
        
        if let footerConfiguration = section.footerConfiguration {
            tableView?.registerCell(type: footerConfiguration.headerFooterType)
        }
    }
    
    func registerCellType(_ cellType: UITableViewCell.Type) {
        tableView?.registerCell(type: cellType)
    }
    
    func performTableMutations(mutations: @escaping (() -> Void),
                               reloadAnimation: UITableView.RowAnimation) {
        let mutationAction: (() -> Void) = {
            self.tableView?.beginUpdates()
            mutations()
            self.tableView?.endUpdates()
        }
        
        if reloadAnimation == UITableView.RowAnimation.none {
            UIView.performWithoutAnimation {
                mutationAction()
            }
        } else {
            mutationAction()
        }
    }
}
