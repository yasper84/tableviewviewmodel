//
//  ConfigureTableViewMixin.swift
//
//  Created by Jasper Siebelink on 02/01/2019.
//  Copyright © 2019 Jasper Siebelink. All rights reserved.
//

import UIKit

// Configurator meta
extension UITableView {
    private struct Constants {
        static var viewModelKey: UInt8 = 100
    }
    
    public var viewModel: TableViewViewModel? {
        get {
            return objc_getAssociatedObject(self, &Constants.viewModelKey) as? TableViewViewModel
        }
        set {
            if newValue == nil && viewModel != nil {
                delegate = nil
                dataSource = nil
                reloadData()
            }
            
            if let newViewModel = newValue {
                newViewModel.configure(tableView: self)
            }
            
            objc_setAssociatedObject(self,
                                     &Constants.viewModelKey,
                                     newValue,
                                     objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN_NONATOMIC)
        }
    }
}
