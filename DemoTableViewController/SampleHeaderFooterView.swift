//
// YLTUIKitDemo
//
// Created by Jasper Siebelink on 15/03/2019
// Copyright Yolt. All rights reserved.
// 

import UIKit
import CommonExtensions

final class SampleHeaderFooterViewModel: Equatable {
    let someText: String

    init(someText: String) {
        self.someText = someText
    }
    
    static func == (lhs: SampleHeaderFooterViewModel, rhs: SampleHeaderFooterViewModel) -> Bool {
        return lhs.someText == rhs.someText
    }
}

final class SampleHeaderFooterView: UITableViewHeaderFooterView, ConfigurableTableViewHeaderFooter {

    func configure(with viewModel: SampleHeaderFooterViewModel) {
        let label: UILabel = UILabel()
        label.text = viewModel.someText
        label.textAlignment = .center
        addSubview(label)
        label.pinHeight(40)
        label.pinToSuperview()
        label.textColor = UIColor.white

        contentView.backgroundColor = UIColor.purple
    }
}
