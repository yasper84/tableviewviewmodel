//
//  MyCellViewModel.swift
//  GenericList
//
//  Created by Jasper Siebelink on 04/01/2019.
//  Copyright © 2019 Jasper Siebelink. All rights reserved.
//

import UIKit

class MyTableViewCellViewModel: NSObject {
    let title: String
    let color: UIColor

    init(title: String, color: UIColor) {
        self.title = title
        self.color = color
    }
}
