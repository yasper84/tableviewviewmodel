//
//  StringTableViewCell.swift
//  GenericList
//
//  Created by Jasper Siebelink on 06/01/2019.
//  Copyright © 2019 Jasper Siebelink. All rights reserved.
//

import UIKit

class StringTableViewCell: UITableViewCell, ConfigurableTableViewCell {
    @IBOutlet private weak var colorView: UIView!
    @IBOutlet private weak var contentLabel: UILabel!

    func configure(with viewModel: String) {
        contentLabel.text = "StringViewModel: \(viewModel)"
    }
}
