//
//  ViewModelTableViewCell
//  GenericList
//
//  Created by Jasper Siebelink on 04/01/2019.
//  Copyright © 2019 Jasper Siebelink. All rights reserved.
//

import UIKit

class ViewModelTableViewCell: UITableViewCell, ConfigurableTableViewCell {
    static var cellHeight: CGFloat = 80

    @IBOutlet private weak var colorView: UIView!
    @IBOutlet private weak var contentLabel: UILabel!

    func configure(with viewModel: MyTableViewCellViewModel) {
        contentLabel.text = "CellViewModel: \(viewModel.title)"
        colorView.backgroundColor = viewModel.color
    }
}
