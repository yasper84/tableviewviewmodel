//
//  MyTableViewModel.swift
//  GenericList
//
//  Created by Jasper Siebelink on 19/01/2019.
//  Copyright © 2019 Jasper Siebelink. All rights reserved.
//

import UIKit

class MyTableViewModel: NSObject {
    var randomView: UIView {
        let randomView = UIView()
        randomView.backgroundColor = UIColor.yellow
        randomView.frame = CGRect(x: 0,
                                  y: 0,
                                  width: UIScreen.main.bounds.size.width,
                                  height: 50)
        return randomView
    }

    var randomCellViewModel: MyTableViewCellViewModel {
        return MyTableViewCellViewModel(title: "\(arc4random()%100)", color: UIColor.blue)
    }

    var variedViewModels: [Any] {
        return ["Bla",
                randomCellViewModel,
                randomCellViewModel,
                "Bla2",
                randomCellViewModel]
    }

    func singleSection(withHeaderFooter: Bool = false) -> TableViewSectionViewModel {
        return TableViewSectionViewModel(viewModels: [randomCellViewModel,
                                                      randomCellViewModel,
                                                      randomCellViewModel],
                                         cellType: ViewModelTableViewCell.self)
    }

    func multiSectionViewModels(withHeaderFooter: Bool = false) -> [TableViewSectionViewModel] {
        return [singleSection(withHeaderFooter: withHeaderFooter),
                singleSection(withHeaderFooter: withHeaderFooter),
                singleSection(withHeaderFooter: withHeaderFooter)]
    }
}
