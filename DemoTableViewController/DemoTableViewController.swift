//
//  DemoTableViewController
//  GenericList
//
//  Created by Jasper Siebelink on 02/01/2019.
//  Copyright © 2019 Jasper Siebelink. All rights reserved.
//

import UIKit

// Advantages:
// - No boilerplate code
// - Automatic celltype registration
// - Easy add/delete/replace for tableview (i.e. insertion animations etc)
// - OnClick callback with already-casted viewmodel
// - Configuration called in your TableViewCell with already-casted viewmodel
// - Items can be stored inside TableViewViewModel; no need to hang on to them
// - Portable TableView ViewModel easily transferrable to another tableview

class DemoTableViewController: UIViewController {
    @IBOutlet private weak var tableView: UITableView!
    @IBOutlet private weak var descriptionLabel: UILabel!
    private let viewModel = MyTableViewModel()
    
    private var timer: Timer?
    private func delayed(block: @escaping (() -> Void), timeDelay: TimeInterval = 3) {
        timer?.invalidate()
        timer = Timer.scheduledTimer(withTimeInterval: timeDelay,
                                     repeats: false,
                                     block: { (_) in
                                        block()
        })
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        // Cycle through some example usages
        headerFooterExample()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        timer?.invalidate()
    }
    
    private func headerFooterExample() {
        tableView.viewModel = TableViewViewModel()
        
        let cellViewmodel: MyTableViewCellViewModel = MyTableViewCellViewModel(
            title: "\(arc4random()%100)",
            color: UIColor.blue)
        
        // With header
        let section = TableViewSectionViewModel(headerViewModel: SampleHeaderFooterViewModel(someText: "Some header text!!"),
                                                headerType: SampleHeaderFooterView.self,
                                                viewModels: [cellViewmodel],
                                                cellType: ViewModelTableViewCell.self,
                                                cellHeight: 40)
        tableView.viewModel?.append(section: section,
                                    reloadAnimation: UITableView.RowAnimation.fade)
        descriptionLabel.text = "Section with header"
        
        // With footer
        delayed(block: {
            let footerSection = TableViewSectionViewModel(viewModels: [cellViewmodel],
                                                          cellType: ViewModelTableViewCell.self,
                                                          cellHeight: 40,
                                                          footerViewModel: SampleHeaderFooterViewModel(someText: "Some footer text!!"),
                                                          footerType: SampleHeaderFooterView.self)
            self.tableView.viewModel?.updateSections([footerSection])
            self.descriptionLabel.text = "Section with footer"

            self.delayed(block: {
                let headerFooterSection = TableViewSectionViewModel(headerViewModel: SampleHeaderFooterViewModel(someText: "Some header text!!"),
                                                                    headerType: SampleHeaderFooterView.self,
                                                                    viewModels: [cellViewmodel],
                                                                    cellType: ViewModelTableViewCell.self,
                                                                    cellHeight: 40,
                                                                    footerViewModel: SampleHeaderFooterViewModel(someText: "Some footer text!!"),
                                                                    footerType: SampleHeaderFooterView.self)
                self.tableView.viewModel?.updateSections([headerFooterSection])
                self.descriptionLabel.text = "Section with header&footer"

                self.delayed(block: {
                    self.showMixedSection()
                })
            })
        })
    }
    
    private func showMixedSection() {
        let tableViewViewModel = TableViewViewModel()
        var section: TableViewSectionViewModel = TableViewSectionViewModel()
        section.append(viewModel: "1",
                       cellType: StringTableViewCell.self)
        section.append(viewModel: "2",
                       cellType: StringTableViewCell.self)
        section.append(viewModel: viewModel.randomCellViewModel,
                       cellType: ViewModelTableViewCell.self)
        tableViewViewModel.append(section: section)
        tableView.viewModel = tableViewViewModel
        
        descriptionLabel.text = "TableView with multiple different cells"
        
        delayed(block: {
            self.showMultiSection()
        })
    }
    
    private func showMultiSection() {
        tableView.viewModel = TableViewViewModel()
        tableView.viewModel?.append(sections: viewModel.multiSectionViewModels(withHeaderFooter: true),
                                    reloadAnimation: UITableView.RowAnimation.right)
        
        descriptionLabel.text = "TableView with multiple sections"
        
        delayed(block: {
            self.showOnClick()
        })
    }
    
    private func showOnClick() {
        tableView.viewModel = TableViewViewModel()
        
        let section = TableViewSectionViewModel(viewModels: [viewModel.randomCellViewModel,
                                                             viewModel.randomCellViewModel,
                                                             viewModel.randomCellViewModel],
                                                cellType: ViewModelTableViewCell.self,
                                                onCellClicked: { (viewModel, indexPath) in
                                                    print("Clicked on: \(viewModel.title) at \(indexPath.row)")
        })
        tableView.viewModel?.append(section: section,
                                    reloadAnimation: UITableView.RowAnimation.fade)
        
        // Simulate some clicks
        delayed(block: {
            Array(0...2).forEach({ (rowPosition) in
                let selectIndexPath = IndexPath(row: rowPosition,
                                                section: 0)
                self.tableView.delegate?.tableView!(self.tableView,
                                                    didSelectRowAt: selectIndexPath)
            })
            
            self.delayed(block: {
                self.showInsertRemove()
            })
        }, timeDelay: 1)
        
        descriptionLabel.text = "Callback for cells"
    }
    
    private func showInsertRemove() {
        tableView.viewModel = TableViewViewModel()
        tableView.viewModel?.append(sections: viewModel.multiSectionViewModels(withHeaderFooter: true),
                                    reloadAnimation: UITableView.RowAnimation.right)
        
        // Apply append/remove
        delayed(block: {
            self.tableView.viewModel?.remove(indexPath: IndexPath(row: 2,
                                                                  section: 1),
                                             reloadAnimation: UITableView.RowAnimation.right)
            self.delayed(block: {
                self.tableView?.viewModel?.remove(indices: [IndexPath(row: 0, section: 0),
                                                            IndexPath(row: 1, section: 1),
                                                            IndexPath(row: 2, section: 2)],
                                                  reloadAnimation: .fade)
                self.delayed(block: {
                    let insertCellConfiguration = TableViewCellConfiguration(cellViewModel: "1",
                                                                             type: StringTableViewCell.self)
                    self.tableView?.viewModel?.append(cellConfigurator: insertCellConfiguration,
                                                      indexPath: IndexPath(row: 1,
                                                                           section: 2),
                                                      reloadAnimation: UITableView.RowAnimation.left)
                    self.delayed(block: {
                        self.tableView.viewModel?.remove(sectionIndices: [1, 2],
                                                         reloadAnimation: UITableView.RowAnimation.fade)
                        self.delayed(block: {
                            self.animatedFullReplaceSection()
                        }, timeDelay: 1)
                    }, timeDelay: 1)
                }, timeDelay: 1)
            }, timeDelay: 1)
        }, timeDelay: 1)
        
        descriptionLabel.text = "Replace existing items"
    }
    
    private func animatedFullReplaceSection() {
        let firstSectionViewModel = TableViewSectionViewModel(viewModels: ["1", "2", "3"],
                                                              cellType: StringTableViewCell.self,
                                                              cellHeight: 50)
        let tableViewModel = TableViewViewModel(sections: [firstSectionViewModel])
        tableView.viewModel = tableViewModel
        
        delayed(block: {
            let newSectionViewModel = TableViewSectionViewModel(viewModels: ["4", "1", "5", "6", "2", "7", "3"],
                                                                cellType: StringTableViewCell.self,
                                                                cellHeight: 50)
            tableViewModel.updateSection(newSectionViewModel: newSectionViewModel,
                                         sectionIndex: 0,
                                         reloadAnimation: UITableView.RowAnimation.fade)
            self.delayed(block: {
                let newSectionViewModel = TableViewSectionViewModel(viewModels: ["1", "5", "2", "7", "3"],
                                                                    cellType: StringTableViewCell.self,
                                                                    cellHeight: 50)
                tableViewModel.updateSection(newSectionViewModel: newSectionViewModel,
                                             sectionIndex: 0,
                                             reloadAnimation: UITableView.RowAnimation.fade)
                self.delayed(block: {
                    let newSectionViewModel = TableViewSectionViewModel(viewModels: ["1", "5"],
                                                                        cellType: StringTableViewCell.self,
                                                                        cellHeight: 50)
                    tableViewModel.updateSection(newSectionViewModel: newSectionViewModel,
                                                 sectionIndex: 0,
                                                 reloadAnimation: UITableView.RowAnimation.fade)
                    self.delayed(block: {
                        let newSectionViewModel = TableViewSectionViewModel(viewModels: ["1", "2", "3", "4", "5"],
                                                                            cellType: StringTableViewCell.self,
                                                                            cellHeight: 50)
                        tableViewModel.updateSection(newSectionViewModel: newSectionViewModel,
                                                     sectionIndex: 0,
                                                     reloadAnimation: UITableView.RowAnimation.fade)
                        
                        self.delayed(block: {
                            self.showSectionKeys()
                        }, timeDelay: 3)
                    }, timeDelay: 1)
                }, timeDelay: 1)
            }, timeDelay: 1)
        }, timeDelay: 1)
        
        descriptionLabel.text = "Replace entire section animated"
    }
    
    private func showSectionKeys() {
        let sections: [TableViewSectionViewModel] = Array(0...20).map { (interval) -> TableViewSectionViewModel in
            let sectionKey: String = "\(interval)"
            return TableViewSectionViewModel(
                viewModels: [
                    "\(interval): a",
                    "\(interval): b",
                    "\(interval): c",
                    "\(interval): d"],
                cellType: StringTableViewCell.self,
                sectionKey: sectionKey)
        }
        tableView.viewModel = TableViewViewModel(sections: sections)
        
        descriptionLabel.text = "Section keys"
    }
}
